betterrevApp.controller('navigationController', ['$scope', 'authenticationService',
        function ($scope, authenticationService) {

            $scope.signInRequired = function () {
                return authenticationService.isAuthenticated() === false;
            };
        }]
);
