betterrevApp.directive('signIn', ['authenticationService', function (authenticationService) {

    return {
        restrict: 'E',
        templateUrl: 'partials/signin.html',
        replace: true,
        scope: {},
        link: function (scope) {

            scope.authenticate = function () {
                authenticationService.authenticate();
            };

            scope.signInRequired = function () {
                return !authenticationService.isAuthenticated();
            };

            scope.userDetails = authenticationService.getUserDetails();
        }
    };
}]);